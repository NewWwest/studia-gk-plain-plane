export class TerrainModelData {

    constructor(MapModelSize: number,
        HorizontalResolution: number,
        VerticalResolution: number) {
            this.mapModelWidth = MapModelSize;
            this.mapModelHeight = MapModelSize;
            this.horizontalResolution = HorizontalResolution;
            this.verticalResolution = VerticalResolution;
            this.terrainRenderType = TerrainRenderType.LambertDefault;
            this.useFog = true;
        }

    terrainRenderType: TerrainRenderType;
    mapModelWidth: number;
    mapModelHeight: number;
    horizontalResolution: number;
    verticalResolution: number;
    originX: number;
    originY: number;
    useFog: boolean;
}

export enum TerrainRenderType {
    LambertDefault,
    GradientPlain,
    PhongCustom,
    BlinnCustom
}
