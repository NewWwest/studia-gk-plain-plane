export class EnvironmentConfig {
    sunVisible: boolean;
    fogVisible: boolean;
    rainVisible: boolean;
    constructor() {
        this.sunVisible = true;
        this.fogVisible = true;
        this.rainVisible = false;
    }
}
export enum EnviromentElementChanged {
    Sun,
    Fog,
    Rain
}
