export class TerrainConfig {
    octaveRatios: number[];
    seed: number;
    
    constructor(seed= 1, ratio1= 1, ratio2= 0.5, ratio3= 0.25, ratio4= 0.125, ratio5= 0.06) {
        this.seed = seed;
        this.octaveRatios = [ratio1, ratio2, ratio3, ratio4, ratio5];
        console.log(this.octaveRatios);
    }
}
