import * as THREE from 'three-full';
import { CameraType } from '../enums/cameraType';
import { Vector3 } from 'three-full';

export class CameraService {

    Get(cameraType: CameraType, planePosition: Vector3, planeDirection: Vector3, planeScale: Vector3): THREE.PerspectiveCamera {
        switch (cameraType) {
            case CameraType.FirstPersonCamera:
                return this.getCamera(planeDirection.clone().add(planePosition), planePosition);
                
            case CameraType.ThirdPersonCamera:
                const newPosition = planePosition.clone();
                const newDirection = planeDirection.clone();
                newDirection.add(planePosition);
                newPosition.sub(planeDirection);
                newPosition.add(new Vector3(0, planeScale.y * 30, 0));
            return this.getCamera(newDirection, newPosition);

            case CameraType.StaticCamera:
                return this.getCamera(new THREE.Vector3(0, 30, 0), new THREE.Vector3(25, 25, 25));

            case CameraType.FollowingCamera:
                return this.getCamera(planePosition, new THREE.Vector3(200, 200, 200));

            default:
                throw new Error();
        }
    }


    getCamera(lookAt: THREE.Vector3, position: THREE.Vector3): THREE.PerspectiveCamera {
        const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
        camera.lookAt(lookAt);
        return camera;
    }
}
