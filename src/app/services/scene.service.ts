import * as THREE from 'three-full';
import { MapService } from './map.service';
import { TerrainConfig } from '../models/terrainConfig.model';
import { TerrainModelData, TerrainRenderType } from '../models/terrainModelData.model';
import { Configuration } from '../components/configuration.const';
import { GradientPhongShader } from '../shaders/terrain/gradientPhongShader';
import { PlainGradientShader } from '../shaders/terrain/plainGradientShader';
import { GradientBlinnPhongShader } from '../shaders/terrain/gradientBlinnPhongShader';
export class SceneService {
  particles: THREE.Geometry;
  particlesSpeed: number[];
  particleSystem: THREE.Object3D;

  constructor() {
  }
  
  helperAxes(): THREE.Object3D[] {
    return [this.addAxis(0xff0000, 500, 0, 0),
      this.addAxis(0xaa0000, -500, 0, 0),

      this.addAxis(0x00ff00, 0, 500, 0),
      this.addAxis(0x00aa00, 0, -500, 0),
      
      this.addAxis(0x0000ff, 0, 0, 500),
      this.addAxis(0x0000aa, 0, 0, -500)];
  }

  helperPlane(): THREE.Object3D {
    const geometry = new THREE.PlaneGeometry( 200, 200, 200 );
    const material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
    const mesh = new THREE.Mesh(geometry, material);
    mesh.rotateX(Math.PI / 2);
    return mesh;
  }

  terrain(terrainModelData: TerrainModelData, terrainConfig: TerrainConfig, camera: THREE.Camera): THREE.Object3D {
    const _mapService: MapService = new MapService(terrainConfig, 
      terrainModelData.mapModelWidth, 
      terrainModelData.mapModelHeight);

    const geometry = new THREE.PlaneGeometry(terrainModelData.mapModelWidth,
        terrainModelData.mapModelHeight,
        terrainModelData.horizontalResolution,
          terrainModelData.verticalResolution);

    geometry.translate(terrainModelData.originX, terrainModelData.originY, 0);
    for (let y = 0; y <= terrainModelData.verticalResolution; y++) {
        for (let x = 0; x <= terrainModelData.horizontalResolution; x++) {
            geometry.vertices[y * terrainModelData.verticalResolution + x].z = _mapService.getHeight(
              geometry.vertices[y * terrainModelData.verticalResolution + x].x, 
              geometry.vertices[y * terrainModelData.verticalResolution + x].y) * Configuration.mountainHeight;
        }
    }
    geometry.computeVertexNormals();
    geometry.computeFaceNormals();

    let material = undefined;
    let uniform = undefined;
    switch (terrainModelData.terrainRenderType) {
      case TerrainRenderType.LambertDefault:
      material = new THREE.MeshLambertMaterial({
        side: THREE.DoubleSide,
        color: 'blue'
      });
        break;
      case TerrainRenderType.GradientPlain:
      uniform = new THREE.Uniform({});
        material = new THREE.ShaderMaterial({
          uniforms: uniform,
          vertexShader: PlainGradientShader.vertexShader(),
          fragmentShader: PlainGradientShader.fragmentShader(),
          side: THREE.DoubleSide,
        });
        break;

      case TerrainRenderType.PhongCustom:
        material = new THREE.ShaderMaterial({
          uniforms: uniform,
          vertexShader: GradientPhongShader.vertexShader(),
          fragmentShader: GradientPhongShader.fragmentShader(terrainModelData.useFog),
          side: THREE.DoubleSide,
        });
        break;
    
        case TerrainRenderType.BlinnCustom:
        uniform = new THREE.Uniform({});
        material = new THREE.ShaderMaterial({
          uniforms: uniform,
          vertexShader: GradientBlinnPhongShader.vertexShader(),
          fragmentShader: GradientBlinnPhongShader.fragmentShader(terrainModelData.useFog),
          side: THREE.DoubleSide,
        });
        break;
      default:
        break;
    }
    
    const plane = new THREE.Mesh(geometry, material);
    plane.rotateX(-Math.PI / 2);
    
    return plane;  
  }
      
  private addAxis(color: number, x: number, y: number, z: number): THREE.Object3D {
    const material = new THREE.LineBasicMaterial({
      color: color 
    });
    
    const geometry = new THREE.Geometry();
    geometry.vertices.push(
      new THREE.Vector3( 0, 0, 0 ),
      new THREE.Vector3( x, y, z )
    );
    
    const line = new THREE.Line( geometry, material );
    return line;
  }

  initSky(): THREE.Object3D[] {
    const sky = new (<any>THREE).Sky();
    sky.scale.setScalar( 450000 );

    // Add Sun Helper
    const sunSphere = new THREE.Mesh(
      new THREE.SphereBufferGeometry( 20000, 16, 8 ),
      new THREE.MeshBasicMaterial( { color: 0xffffff } )
    );
    const distance = 400000;
    const theta = -Math.PI * 0.05;
    const phi = Math.PI * -0.5;
    sunSphere.position.x = distance * Math.cos( phi );
    sunSphere.position.y = distance * Math.sin( phi ) * Math.sin( theta );
    sunSphere.position.z = distance * Math.sin( phi ) * Math.cos( theta );
    sunSphere.visible = false;
    const uniforms = sky.material.uniforms;
    uniforms[ "turbidity" ].value = 9;
    uniforms[ "rayleigh" ].value = 2;
    uniforms[ "luminance" ].value = 1;
    uniforms[ "mieCoefficient" ].value = 0.006;
    uniforms[ "mieDirectionalG" ].value = 0.7;
    uniforms[ "sunPosition" ].value.copy( sunSphere.position );

    return [sky, sunSphere];
  }

  initRain() {
    if (this.particleSystem != undefined) {
      this.particles = undefined;
      this.particleSystem = undefined;
      return undefined;
    }
    const material = new THREE.PointsMaterial( { color: 0xffffff } );

    this.particles = new THREE.Geometry;
    for (let i = 0; i < Configuration.particleCount; i++) {
        const pX = Math.random() * 500 - 250,
            pY = Math.random() * 500 - 250,
            pZ = Math.random() * 500 - 250,
            particle = new THREE.Vector3(pX, pY, pZ);
        this.particles.vertices.push(particle);
      }
    this.particleSystem = new THREE.Points(this.particles, material);

    return this.particleSystem;
  }

  manageRain() {
    if (this.particleSystem == undefined) {
      return ;
    }

    this.particleSystem.rotation.y += 0.0015;

    let pCount = Configuration.particleCount;
    while (pCount--) {
    if (this.particles.vertices[pCount].y < 0) {
      this.particles.vertices[pCount].y = Configuration.mountainHeight;
    }
    this.particles.vertices[pCount].y -= Math.random() * 1;
    }
    this.particles.verticesNeedUpdate = true;
  }
}
