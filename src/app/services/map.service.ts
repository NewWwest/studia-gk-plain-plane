import { TerrainConfig } from '../models/terrainConfig.model';
declare var Noise: any;

export class MapService {
  _noiseGenerator: any;
  WidthScale: number = 100;
  HeightScale: number = 100;
    
  constructor(private terrainConfig: TerrainConfig, private width: number, private height: number) {
    this._noiseGenerator = new Noise(terrainConfig.seed);
  }

  getHeight(x: number, y: number): number {
      const normalizedX = Math.sin(x / this.WidthScale);
      const normalizedY = Math.sin(y / this.HeightScale);
      return this.octaveNoise(normalizedX, normalizedY);
  }

  noise(nx: number, ny: number) {
    return this._noiseGenerator.simplex2(nx, ny) / 2 + 0.5;
  }

  octaveNoise(nx: number, ny: number) {
    let e = 0;
    let ratioSum = 0;
    for (let i = 0; i < this.terrainConfig.octaveRatios.length; i++) {
      e += this.terrainConfig.octaveRatios[i] * this.noise( Math.pow(2, i) * nx,  Math.pow(2, i) * ny) ;
      ratioSum += this.terrainConfig.octaveRatios[i];
    }

    e = e / ratioSum;
    e = Math.pow(e, 5.00);
    return e;
  }
      
}
