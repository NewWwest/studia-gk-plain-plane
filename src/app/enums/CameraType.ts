export enum CameraType {
    StaticCamera,
    FirstPersonCamera,
    ThirdPersonCamera,
    FollowingCamera
}
