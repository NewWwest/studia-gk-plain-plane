export enum ShadingModel {
    Gouraud,
    Phong,
    Default
}
