import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SceneService } from './services/scene.service';
import { MenuComponent } from './components/menu.component';
import { CameraService } from './services/camera.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule 
  ],
  providers: [
    SceneService,
    CameraService],
  bootstrap: [AppComponent]
})
export class AppModule { }
