export class Configuration {
    static mountainHeight: number = 300;
    static particleCount: number = 14000;
}
