import { Component, Output, EventEmitter } from '@angular/core';
import * as THREE from 'three-full';
import { FormsModule } from '@angular/forms';
import { CameraType } from '../enums/cameraType';
import { TerrainConfig } from '../models/terrainConfig.model';
import { ShadingModel } from '../enums/ShadingModel';
import { EnvironmentConfig, EnviromentElementChanged } from '../models/environmentConfig.model';
import { TerrainRenderType } from '../models/terrainModelData.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  CameraTypeEnum = CameraType;
  currentCameraType: CameraType;
  @Output() CameraTypeChanged = new EventEmitter<CameraType>();

  TerrainConfig = new TerrainConfig();
  @Output() RequiredTerrainRerender = new EventEmitter<TerrainConfig>();
  terrainSeed: number = 2;
  terrainOctave1: number = 1;
  terrainOctave2: number = 0.5;
  terrainOctave3: number = 0.25;
  terrainOctave4: number = 0.125;
  terrainOctave5: number = 0.06;

  @Output() ChangeShadingModel = new EventEmitter<ShadingModel>();
  ShadingModelEnum = ShadingModel;
  currentShadingModel: ShadingModel = ShadingModel.Default;

  @Output() ChangeEnviromentElement = new EventEmitter<EnviromentElementChanged>();
  environmentConfig: EnvironmentConfig = new EnvironmentConfig();
  EnviromentElementChangedEnum = EnviromentElementChanged;
  
  TerrainRenderTypeEnum = TerrainRenderType;
  terrainRenderType: TerrainRenderType = TerrainRenderType.LambertDefault;
  @Output() ChangeTerrainRenderType = new EventEmitter<TerrainRenderType>();

  constructor() {
    this.currentCameraType = CameraType.FollowingCamera;
  }

  changeCamera(cameraType: CameraType) {
    if (cameraType == this.currentCameraType) {
    return;
    }

    this.currentCameraType = cameraType;
    this.CameraTypeChanged.emit(cameraType);
  }

  terrainFormSubmit() {
    if (
      this.terrainSeed == this.TerrainConfig.seed &&
      this.terrainOctave1 == this.TerrainConfig.octaveRatios[0] &&
      this.terrainOctave2 == this.TerrainConfig.octaveRatios[1] &&
      this.terrainOctave3 == this.TerrainConfig.octaveRatios[2] &&
      this.terrainOctave4 == this.TerrainConfig.octaveRatios[3] &&
      this.terrainOctave5 == this.TerrainConfig.octaveRatios[4]
    ) {
      console.log("same terrain values - no changes apply");
      return;
    }

    this.TerrainConfig.seed = this.terrainSeed;
    this.TerrainConfig.octaveRatios[0] = this.terrainOctave1;
    this.TerrainConfig.octaveRatios[1] = this.terrainOctave2;
    this.TerrainConfig.octaveRatios[2] = this.terrainOctave3;
    this.TerrainConfig.octaveRatios[3] = this.terrainOctave4;
    this.TerrainConfig.octaveRatios[4] = this.terrainOctave5;
    this.RequiredTerrainRerender.emit(this.TerrainConfig);

  }


  applyShading(shadingModel: ShadingModel) {
    if (this.currentShadingModel == shadingModel) {
      return;
    }
    this.currentShadingModel = shadingModel;
    this.ChangeShadingModel.emit(this.currentShadingModel);
  }


  changeEnvironment(changed: EnviromentElementChanged) {
    this.ChangeEnviromentElement.emit(changed);
  }

  applyTerrainStyle(newRenderType: TerrainRenderType) {
    if (this.terrainRenderType == newRenderType) {
      return;
    }
    this.terrainRenderType = newRenderType;
    this.ChangeTerrainRenderType.emit(newRenderType);
  }
}
