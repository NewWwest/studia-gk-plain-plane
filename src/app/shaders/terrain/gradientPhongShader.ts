import { Configuration } from '../../components/configuration.const';

export class GradientPhongShader {
    static vertexShader() {
        return `
        varying vec3 vUv; 
        varying vec3 normalInterp;
        varying vec3 vertPos;
        varying float distance2camera;
        
        void main(){
            vec4 vertPos4 = modelViewMatrix  * vec4(position, 1.0);

            vUv = position; 
            vertPos = vec3(vertPos4) / vertPos4.w;
            normalInterp = vec3(normalMatrix * normal);
            distance2camera = -vertPos4.z;

            gl_Position = projectionMatrix  * vertPos4;
        }
        `;
    }

    static fragmentShader(useFog: boolean) {
        return `
        varying vec3 vUv;
        varying vec3 normalInterp;
        varying vec3 vertPos;
        varying float distance2camera;
        
        const vec3 lightPos = vec3(0, 62573, -395075);
        const vec3 lightColor = vec3(1.0, 1.0, 0.9);
        const float lightPower = 5.0;
        const vec3 ambientColor = vec3(0.03, 0.03, 0.03);
        const vec3 diffuseColor = vec3(0.5, 0.5, 0.5);
        const vec3 specColor = vec3(0.05, 0.05, 0.05);
        const float shininess = 0.2;
        const float screenGamma = 2.2; 

        const vec3 colorA = vec3(56, 81, 48)/255.0;
        const vec3 colorB = vec3(50,175,50)/255.0;
        const vec3 colorC = vec3(204,204,204)/255.0;

        void main() {
          vec3 normal = normalize(normalInterp);
          float distance = ${useFog ? "distance2camera / 30.0" : "1.0" };

          vec3 lightDir = lightPos - vertPos;
          lightDir = normalize(lightDir);
      
          float lambertian = max(dot(lightDir,normal), 0.0);
          float specular = 0.0;
        
          if(lambertian > 0.0) {
            vec3 viewDir = normalize(-vertPos);
            vec3 reflectDir = reflect(-lightDir, normal);
            float  specAngle = max(dot(reflectDir, viewDir), 0.0);
            specular = pow(specAngle, shininess);
          }
          
          vec3 fragmentColor;
          if(vUv.z < float(${Configuration.mountainHeight / 20}))
            fragmentColor = vec3(mix(colorA, colorB, (vUv.z/40.0) ));
          else
            fragmentColor = vec3(mix(colorB, colorC, (vUv.z - float(${Configuration.mountainHeight / 20}))/60.0 ));

          vec3 colorLinear = ambientColor +
            diffuseColor * lambertian * lightColor * lightPower  +
            specColor * specular * lightColor * lightPower;

          colorLinear= colorLinear * fragmentColor * distance;

          vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0/screenGamma));
          gl_FragColor= vec4(colorGammaCorrected,1.0);
        }
`;
    }
}
