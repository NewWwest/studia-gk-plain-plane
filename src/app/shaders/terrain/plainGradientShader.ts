import { Configuration } from '../../components/configuration.const';

export class PlainGradientShader {
    static vertexShader() {
        return `
        varying vec3 vUv; 
        
        void main(){
            vUv = position; 

            vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
            gl_Position = projectionMatrix  * modelViewMatrix  * vec4(position, 1.0);
            gl_Position = projectionMatrix  * modelViewPosition;
        }
        `;
    }

    static fragmentShader() {
        return `
        varying vec3 vUv;

        const vec3 colorA = vec3(56, 81, 48)/255.0;
        const vec3 colorB = vec3(50,175,50)/255.0;
        const vec3 colorC = vec3(204,204,204)/255.0;

        void main() {

          vec3 fragmentColor;

          if(vUv.z < float(${Configuration.mountainHeight / 20}))
            fragmentColor = vec3(mix(colorA, colorB, (vUv.z/40.0) ));
          else
            fragmentColor = vec3(mix(colorB, colorC, (vUv.z - float(${Configuration.mountainHeight / 20}))/60.0 ));

          gl_FragColor= vec4(fragmentColor,1.0);
        }
`;
    }
}
