export class GouraudShading {
    static vertexShader(): string {
        return `
const float shininessVal = 2.0;
const vec3 lightPos = vec3(0, 62573, -395075);
const float Ka = 1.0;
const float Kd = 1.0;
const float Ks = 1.0;

const vec3 ambientColor = vec3(10.0, 10.0, 10.0)/255.0;
const vec3 diffuseColor = vec3(255.0, 255.0, 255.0)/255.0;
const vec3 specularColor = vec3(50.0, 50.0, 50.0)/255.0;

varying vec4 color; //color

void main(){
    vec4 vertPos4 = modelViewMatrix * vec4(position, 1.0);
    vec3 vertPos = vec3(vertPos4) / vertPos4.w;
    vec3 normalInterp = vec3(normalMatrix * normal);
    gl_Position = projectionMatrix * vertPos4;

    vec3 N = normalize(normalInterp);
    vec3 L = normalize(lightPos - vertPos);
    float lambertian = max(dot(N, L), 0.0);
    float specular = 0.0;

    if(lambertian > 0.0) {
        vec3 R = reflect(-L, N);
        vec3 V = normalize(-vertPos);
        float specAngle = max(dot(R, V), 0.0);
        specular = pow(specAngle, shininessVal);
    }

    color = vec4(Ka * ambientColor +
                Kd * lambertian * diffuseColor +
                Ks * specular * specularColor, 1.0);
}`;
}
    
    static fragmentShader(): string {
    return `
precision mediump float;
varying vec4 color;

void main() {
    gl_FragColor = color;
}`;
}
}
