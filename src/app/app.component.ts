import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as THREE from 'three-full';
import { SceneService } from './services/scene.service';
import { Vector3, Object3D, PointLight, Material } from 'three-full';
import { CameraType } from './enums/cameraType';
import { CameraService } from './services/camera.service';
import { TerrainConfig } from './models/terrainConfig.model';
import { TerrainModelData, TerrainRenderType } from './models/terrainModelData.model';
import { timer } from 'rxjs';
import { ShadingModel } from './enums/ShadingModel';
import { GouraudShading } from './shaders/gouraudShading';
import { EnviromentElementChanged } from './models/environmentConfig.model';
import { PhongShading } from './shaders/phongShading';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('canvas') canvas: ElementRef;
  renderer = new THREE.WebGLRenderer();
  scene: THREE.Scene;
  sunModels: THREE.Object3D[] = undefined;
  rainSystem: THREE.Object3D;

  camera: THREE.PerspectiveCamera;
  cameraType: CameraType = CameraType.FollowingCamera;

  planeModel: THREE.Object3D;
  planeAngleChange: number = 0.05;
  planeAngle: number = 0;
  planeSpeed: number = 0.1;

  planeLight: THREE.SpotLight;
  planeLightRotationY: number = 0;
  planeLightRotationYChange: number = 0.05;
  planeLightRotationZ: number = 0;
  planeLightRotationZChange: number = 0.05;

  terrainModel: THREE.Object3D;
  terrainModelData: TerrainModelData;
  terrainConfig: TerrainConfig = new TerrainConfig();
  
  menuVisible: boolean = true;

  constructor(private _sceneService: SceneService,
    private _cameraService: CameraService) {
    this.scene = new THREE.Scene();
    this.planeAngle = 0;
        
    //this.scene.add(_sceneService.helperPlane());
    this.renderTerrain(this.terrainConfig);
    //_sceneService.helperAxes().forEach(element => this.scene.add(element));
    this.addAirplane();
    this.scene.add( new THREE.AmbientLight( 0x404040, 1 ) );
    const directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
    this.scene.add(directionalLight);
  }

  ngAfterViewInit(): void {
    window.addEventListener('keydown', (event) => {this.keyDownHandle(event); }, false);
    window.addEventListener( 'resize', () => {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    }, false );

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.canvas.nativeElement.appendChild(this.renderer.domElement);

    this.changeEnvironment(EnviromentElementChanged.Fog);
    this.changeEnvironment(EnviromentElementChanged.Sun);

    this.animate();

    timer(0, 1000).subscribe((val: any) => {
      if (this.planeCloseToEdge()) {
        console.log('rendering'); //this accually fixes a bug
        this.renderTerrain(this.terrainConfig);
      }
    });
  }

  animate() {
    window.requestAnimationFrame(() => {
      if (this.planeModel != undefined) {
        const vectorx = new THREE.Vector3();
        this.planeModel.getWorldDirection(vectorx);
        const axis = new THREE.Vector3( 0, 0, 1 );
        const angle = -Math.PI / 2;
        vectorx.applyAxisAngle( axis, angle );
    
        const planeDirection = new Vector3(60 * Math.cos(this.planeAngle), 0, -60 * Math.sin(this.planeAngle));
        const planePosition = this.planeModel.position.clone().add(planeDirection);
        this.planeModel.translateX( this.planeSpeed);
        this.planeModel.rotation.z = this.planeAngle;
    
        this.camera = this._cameraService.Get(this.cameraType, planePosition, planeDirection, this.planeModel.scale);
        this._sceneService.manageRain();
        this.renderer.render(this.scene, this.camera);
      }
      this.animate();
    });
  }

  planeCloseToEdge(): boolean {
    if (this.planeModel == undefined || this.terrainModelData == undefined) {
      return false;
    }
    else {
      return Math.abs(this.terrainModelData.originX - this.planeModel.position.x) > this.terrainModelData.mapModelHeight / 4 ||
      Math.abs(this.terrainModelData.originY - this.planeModel.position.y) > this.terrainModelData.mapModelWidth / 4;
    }
  }

  keyDownHandle(e: KeyboardEvent) {
    if (e.key == 'd') {
      this.planeAngle -= this.planeAngleChange;
    } else if ( e.key == 'a' ) {
      this.planeAngle += this.planeAngleChange;
    }
    else if (e.key == 'ArrowRight') {
      this.planeLightRotationY -= this.planeLightRotationYChange;
    }
    else if (e.key == 'ArrowLeft') {
      this.planeLightRotationY += this.planeLightRotationYChange;
    }
    else if (e.key == 'ArrowUp') {
      this.planeLightRotationZ += this.planeLightRotationZChange;
    }
    else if (e.key == 'ArrowDown') {
      this.planeLightRotationZ -= this.planeLightRotationZChange;
    }
    
    this.ApplyCapOnAngles();
    this.planeLight.target.position.set(60,
      10 * Math.sin(this.planeLightRotationY),
      10 + 10 * Math.cos(this.planeLightRotationZ));
  }

  ApplyCapOnAngles(): any {
    if (this.planeAngle < 0) {
      this.planeAngle += 2 * Math.PI;
    } else if (this.planeAngle > 2 * Math.PI) {
      this.planeAngle -= 2 * Math.PI;
    }

    if (this.planeLightRotationZ < 0) {
      this.planeLightRotationZ += 2 * Math.PI;
    } else if (this.planeLightRotationZ > 2 * Math.PI) {
      this.planeLightRotationZ -= 2 * Math.PI;
    }
    
    if (this.planeLightRotationY < 0) {
      this.planeLightRotationY += 2 * Math.PI;
    } else if (this.planeLightRotationY > 2 * Math.PI) {
      this.planeLightRotationY -= 2 * Math.PI;
    }
  }

  addAirplane(): any {
    const loader = new THREE.GLTFLoader();
    loader.load( '/../../assets/plane.gltf', ( gltf ) => {
      this.planeModel = gltf.scene.children[0];
      this.planeModel.scale.set(0.1, 0.1, 0.1);
      this.planeModel.rotateX(-Math.PI / 2);
      this.planeModel.translateZ(30);
      this.addPlaneLight();
      this.scene.add(this.planeModel);
      }, 
    undefined,
    (error) => {
      console.error( error );
    });
  }

  addPlaneLight(): any {
    const geometry = new THREE.SphereGeometry( 1, 32, 32 );
    const material = new THREE.MeshBasicMaterial( {color: 0x00ff00, visible: false} );
    const sphere = new THREE.Mesh( geometry, material );
    sphere.position.set(50 + 10 * Math.cos(this.planeLightRotationY),
    0,
    10 + 10 * Math.cos(this.planeLightRotationZ));
    
    const light = new THREE.SpotLight( 'white' );
    light.angle = Math.PI / 4;
    light.distance = 200;
    light.penumbra = 0.4;
    this.planeModel.add(light);
    this.planeModel.add(sphere);
    light.translateX(50);
    light.translateZ(10);
    light.target = sphere;
    light.target.updateMatrixWorld(true);
    this.planeLight = light;
  }

  changeCamera(cameraType: CameraType) {
    this.cameraType = cameraType;
  }

  renderTerrain(terrainConfig: TerrainConfig) {
    if (this.terrainModel != undefined) {
      this.scene.remove(this.terrainModel);
    }
    this.terrainConfig = terrainConfig;

    if (this.terrainModelData == undefined) {
      this.terrainModelData = new TerrainModelData(600, 100, 100);
    }

    if (this.planeModel != undefined) {
      this.terrainModelData.originX = this.planeModel.position.x;
      this.terrainModelData.originY = - this.planeModel.position.z;
    } 
    else {
      this.terrainModelData.originX = 0;
      this.terrainModelData.originY = 0;
    }

    this.terrainModel = this._sceneService.terrain(this.terrainModelData, terrainConfig, this.camera);
    this.scene.add(this.terrainModel);
  }
  
  changeShadingModel(shadingModel: ShadingModel) {
    switch (shadingModel) {
      case ShadingModel.Default:
      (<any>this.planeModel).material = new THREE.MeshPhysicalMaterial({
        color: 'silver'
      });
      break;
      case ShadingModel.Gouraud:
        const uniforms1 = new THREE.Uniform({});
        (<any>this.planeModel).material = new THREE.ShaderMaterial({
          uniforms: uniforms1,
          vertexShader: GouraudShading.vertexShader(),
          fragmentShader: GouraudShading.fragmentShader()
        });
      break;
      case ShadingModel.Phong:
      const uniforms2 = new THREE.Uniform({});
      (<any>this.planeModel).material = new THREE.ShaderMaterial({
        uniforms: uniforms2,
        vertexShader: PhongShading.vertexShader(),
        fragmentShader: PhongShading.fragmentShader()
      });
      break;
    }
  }
  
  changeEnvironment(changed: EnviromentElementChanged) {
    switch (changed) {
      case EnviromentElementChanged.Fog:
        if (this.scene.fog == undefined) {
          this.scene.fog = new THREE.Fog(0x111111, 10, 250);
          this.terrainModelData.useFog = true;
        }
        else {
          this.scene.fog = undefined;
          this.terrainModelData.useFog = false;
        }
        this.changeTerrainRenderType(this.terrainModelData.terrainRenderType);
        break;

        case EnviromentElementChanged.Sun:
        if (this.sunModels == undefined) {
          this.sunModels = this._sceneService.initSky();
          this.sunModels.forEach(o => this.scene.add(o));
        }
        else {
          this.sunModels.forEach(o => this.scene.remove(o));
          this.sunModels = undefined;
        }
        break;

        case EnviromentElementChanged.Rain:
          if (this.rainSystem != undefined) {
            this.scene.remove(this.rainSystem);
          }
          this.rainSystem = this._sceneService.initRain();
          if (this.rainSystem != undefined) {
            this.scene.add(this.rainSystem);
          }
          break;
    }
  }

  changeTerrainRenderType(changed: TerrainRenderType) {
    if (this.terrainModelData == undefined) {
      this.terrainModelData = new TerrainModelData(600, 100, 100);
    }
    this.terrainModelData.terrainRenderType = changed;
    this.renderTerrain(this.terrainConfig);
  }
}
